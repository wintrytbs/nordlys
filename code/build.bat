@echo off

set code_dir=%~dp0
set build_dir=%code_dir%..\build
set main_file_name=win32_nordlys.c
set main_file_path=%code_dir%%main_file_name%
set output_name=win32_nordlys.exe
set vk_builder_name=tbs_vk_function_builder
set lib_include_path=%code_dir%..\libs

if not exist %build_dir% (mkdir %build_dir%)


pushd %code_dir%

if exist "shaders" (
if not exist "shaders\spirv" mkdir "shaders\spirv")

echo Compiling shaders...
echo.

glslangValidator -DVERT_SHADER -S vert -V -o shaders\spirv\scene_vert.spv shaders\glsl\scene.glsl
glslangValidator -DFRAG_SHADER -S frag -V -o shaders\spirv\scene_frag.spv shaders\glsl\scene.glsl
glslangValidator -S comp -V -o shaders\spirv\compute.spv shaders\glsl\compute.glsl

echo.
echo Finished Compiling shaders
echo.

pushd %build_dir%

if exist %vk_builder_name%.exe del %vk_builder_name%.exe
clang -xc -std=c11 -Weverything -Werror -O2 -o%vk_builder_name%.exe %code_dir%%vk_builder_name%.c
if not exist %vk_builder_name%.exe goto loader_error

%vk_builder_name%.exe > %code_dir%tbs_vulkan_loader.h


rem These are mostly just during debugging, turn these
rem on once in a while
set clang_ignore_warning=-Wno-unused-parameter
set clang_ignore_warning=-Wno-unused-function %clang_ignore_warning%
set clang_ignore_warning=-Wno-unused-variable %clang_ignore_warning%
set clang_ignore_warning=-Wno-cast-align %clang_ignore_warning%
set clang_ignore_warning=-Wno-c++98-compat %clang_ignore_warning%

rem Nonstandard extension used: nameless struct/union. (For some reason MSVC doesn't think anonymous structs)
rem are standard in c11?
set msvc_ignore_warning=/wd4201
rem Unreferenced formar parameter. (Can be good to enable once in a while to remove old parameters)
set msvc_ignore_warning=/wd4100 %msvc_ignore_warning%
rem Padding added after member. (Can be good to enable once in a while if we want to optimize space usage in structs)
set msvc_ignore_warning=/wd4820 %msvc_ignore_warning%
rem Spectre mitigation inserted if /Qspectre specified
set msvc_ignore_warning=/wd5045 %msvc_ignore_warning%

echo Compiling "%main_file_name%"...
clang -xc -std=c11 -Weverything -Werror -g -O0 -I%lib_include_path% %clang_ignore_warning% -o%output_name% %main_file_path% -luser32
rem cl /std:c11 /Zc:preprocessor /TC /Wall /WX /FC /fsanitize=address /Od /Zi /I %lib_include_path% %msvc_ignore_warning% %main_file_path% /link User32.lib
echo Compilation finished

goto build_finished

:loader_error
echo ERROR ^>^> Failed to create vulkan function loader

:build_finished

popd
popd
