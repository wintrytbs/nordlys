#include <stdio.h>

static const char *global_list[] =
{
    "vkEnumerateInstanceVersion",
    "vkEnumerateInstanceExtensionProperties",
    "vkEnumerateInstanceLayerProperties",
    "vkCreateInstance",
    NULL,
};

static const char *instance_list[] =
{
    "vkEnumeratePhysicalDevices",
    "vkCreateWin32SurfaceKHR",
    "vkGetPhysicalDeviceQueueFamilyProperties",
    "vkGetPhysicalDeviceSurfaceSupportKHR",
    "vkEnumerateDeviceExtensionProperties",
    "vkCreateDevice",
    "vkGetDeviceProcAddr",
    "vkGetPhysicalDeviceSurfaceCapabilitiesKHR",
    "vkGetPhysicalDeviceSurfaceFormatsKHR",
    "vkGetPhysicalDeviceMemoryProperties",
    "vkDestroyInstance",

    // NOTE: DEBUG functionality
    "vkCreateDebugReportCallbackEXT",

    NULL,
};

static const char *device_list[] =
{
    "vkGetDeviceQueue",
    "vkCreateSwapchainKHR",
    "vkCreateRenderPass",
    "vkGetSwapchainImagesKHR",
    "vkCreateImage",
    "vkCreateImageView",
    "vkCreateFramebuffer",
    "vkGetImageMemoryRequirements",
    "vkAllocateMemory",
    "vkBindImageMemory",
    "vkCreateCommandPool",
    "vkAllocateCommandBuffers",
    "vkCmdPipelineBarrier",
    "vkBeginCommandBuffer",
    "vkEndCommandBuffer",
    "vkCreateFence",
    "vkQueueSubmit",
    "vkWaitForFences",
    "vkResetFences",
    "vkResetCommandBuffer",
    "vkCreateShaderModule",
    "vkCreateBuffer",
    "vkGetBufferMemoryRequirements",
    "vkMapMemory",
    "vkUnmapMemory",
    "vkBindBufferMemory",
    "vkCreatePipelineLayout",
    "vkCreateGraphicsPipelines",
    "vkAcquireNextImageKHR",
    "vkCmdBeginRenderPass",
    "vkCmdBindPipeline",
    "vkCmdSetViewport",
    "vkCmdSetScissor",
    "vkCmdBindVertexBuffers",
    "vkCmdDraw",
    "vkCmdEndRenderPass",
    "vkQueuePresentKHR",
    "vkCreateSemaphore",
    "vkDestroyFence",
    "vkDestroySemaphore",
    "vkFlushMappedMemoryRanges",
    "vkDestroyPipeline",
    "vkDestroyPipelineLayout",
    "vkDestroyFramebuffer",
    "vkDestroyImageView",
    "vkFreeMemory",
    "vkDestroyImage",
    "vkDeviceWaitIdle",
    "vkDestroySwapchainKHR",
    "vkCmdCopyBuffer",
    "vkCmdCopyBufferToImage",
    "vkDestroyBuffer",
    "vkCreateSampler",
    "vkCreateDescriptorPool",
    "vkCreateDescriptorSetLayout",
    "vkAllocateDescriptorSets",
    "vkUpdateDescriptorSets",
    "vkCmdBindDescriptorSets",
    "vkCreateComputePipelines",
    "vkCmdDispatch",

    NULL,
};

int main(void)
{
    printf("static PFN_vkGetInstanceProcAddr vkGetInstanceProcAddr;\n");

    int i = 0;
    const char *n = global_list[i++];
    while(n)
    {
        printf("static PFN_%s %s;\n", n, n);
        n = global_list[i++];
    }

    printf("\n");
    printf("static void Vulkan_LoadGlobalFunctions(void)\n");
    printf("{\n");

    i = 0;
    n = global_list[i++];
    while(n)
    {
        printf("    %s = (PFN_%s)vkGetInstanceProcAddr(NULL, \"%s\");\n", n, n, n);
        n = global_list[i++];
    }

    printf("}\n");

    i = 0;
    n = instance_list[i++];
    if(n)
    {
        printf("\n");
        while(n)
        {
            printf("static PFN_%s %s;\n", n, n);
            n = instance_list[i++];
        }
        printf("\n");
        printf("static void Vulkan_LoadInstanceFunctions(VkInstance instance)\n");
        printf("{\n");

        i = 0;
        n = instance_list[i++];
        while(n)
        {
            printf("    %s = (PFN_%s)vkGetInstanceProcAddr(instance, \"%s\");\n", n, n, n);
            n = instance_list[i++];
        }

        printf("}\n");
    }

    i = 0;
    n = device_list[i++];
    if(n)
    {
        printf("\n");
        while(n)
        {
            printf("static PFN_%s %s;\n", n, n);
            n = device_list[i++];
        }
        printf("\n");
        printf("static void Vulkan_LoadDeviceFunctions(VkDevice device)\n");
        printf("{\n");

        i = 0;
        n = device_list[i++];
        while(n)
        {
            printf("    %s = (PFN_%s)vkGetDeviceProcAddr(device, \"%s\");\n", n, n, n);
            n = device_list[i++];
        }

        printf("}\n");
    }
}
