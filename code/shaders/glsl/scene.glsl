#version 450 core

#if defined(VERT_SHADER)
layout (location = 0) in vec2 in_vert;
layout (location = 1) in vec2 in_uv;
layout (location = 2) in vec4 in_col;

layout(location = 0) out vec2 out_uv;
layout(location = 1) out vec4 out_col;

void main()
{
    out_uv = in_uv;
    out_col = in_col;
    gl_Position = vec4(in_vert, 0.0f, 1.0f);
}
#elif defined(FRAG_SHADER)

layout (location = 0) in vec2 in_uv;
layout (location = 1) in vec4 in_col;

layout (location = 0) out vec4 out_col;

layout (binding = 0) uniform sampler2D tex;

void main()
{
    out_col = texture(tex, in_uv);
}

#endif
