#version 450 core

layout (local_size_x = 16, local_size_y = 16) in;
layout (binding = 0, rgba8) uniform writeonly image2D target_image;

struct Circle_t
{
    float x;
    float y;
    float r;

    float pad;
};

layout(std140, binding = 1) buffer circle_buf
{
    uint circle_count;
    Circle_t circles[];
};

void main()
{
    vec2 pos = vec2(gl_GlobalInvocationID.xy);
    vec3 color = vec3(0.0f, 1.0f, 0.0f);
    for(uint i = 0; i < 1; i++)
    {
        vec2 c_pos = vec2(circles[i].x, circles[i].y);
        if(distance(pos, c_pos) < circles[i].r)
        {
            color = vec3(1.0f, 0.0f, 0.0f);
        }
    }
    imageStore(target_image, ivec2(gl_GlobalInvocationID.xy), vec4(color, 0.0f));
}
