#include <stdio.h>
#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

// TODO: Add the lib path to default include path
#define VK_NO_PROTOTYPES
#include "vulkan/include/vulkan.h"
#include "vulkan/include/vulkan_win32.h"

#if defined(__clang__)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wcast-qual"
#pragma clang diagnostic ignored "-Wsign-conversion"
#pragma clang diagnostic ignored "-Wdisabled-macro-expansion"
#pragma clang diagnostic ignored "-Wdouble-promotion"
#pragma clang diagnostic ignored "-Wextra-semi-stmt"
#pragma clang diagnostic ignored "-Wimplicit-int-conversion"
#pragma clang diagnostic ignored "-Wmissing-prototypes"
#pragma clang diagnostic ignored "-Wimplicit-fallthrough"
#endif


#if defined(__clang__)
#pragma clang diagnostic pop
#endif


#include "tbs_vulkan_loader.h"


#define array_count(arr) (sizeof(arr) / sizeof(arr[0]))

typedef int8_t  i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;

typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef float  f32;
typedef double f64;

typedef struct FileData
{
    char *data;
    size_t size;
} FileData;

typedef enum DefaultCmdType
{
    DefaultCmdType_SwapchainSetup,
    DefaultCmdType_DepthSetup,
    DefaultCmdType_Render,
    DefaultCmdType_CopyBuffer,
    DefaultCmdType_ImageLayout,
    DefaultCmdType_Compute,

    DefaultCmdType_COUNT,
} DefaultCmdType;

typedef enum DefaultSemaphores
{
    DefaultSemaphores_RenderDone,
    DefaultSemaphores_PresentDone,

    DefaultSemaphores_COUNT,
} DefaultSemaphores;

typedef struct VulkanContext
{
    // NOTE: base items
    VkInstance instance_handle;
    VkSurfaceKHR surface_handle;
    VkPhysicalDevice gpu_handle;
    VkPhysicalDeviceMemoryProperties gpu_mem_props;
    VkDevice device_handle;
    // TODO: currently we are using the same queue family index
    // for both graphics and compute. This might not be available.
    u32 queue_family_index;
    VkQueue graphics_queue_handle;
    VkQueue compute_queue_handle;

    // NOTE: swapchain related
    VkCommandBuffer default_cmd_buffers[DefaultCmdType_COUNT];
    VkImage *swapchain_images;
    VkImageView *swapchain_image_views;
    u32 swapchain_image_count;
    VkSurfaceFormatKHR *surface_formats;
    u32 surface_format_count;
    // TODO: just store the format and color space
    // directly?
    u32 swapchain_image_format_index;
    VkExtent2D swapchain_extent;
    VkSwapchainKHR swapchain_handle;

    // NOTE: render pass related
    VkRenderPass default_render_pass_handle;
    // NOTE: framebuffer related
    VkFramebuffer *framebuffers;
    u32 framebuffer_count;
    // NOTE: graphics pipeline related
    VkPipelineLayout graphics_pipeline_layout_handle;
    VkPipeline graphics_pipeline_handle;

    // NOTE: compute pipeline related
    VkPipelineLayout compute_pipeline_layout_handle;
    VkPipeline compute_pipeline_handle;

    VkImage depth_image;
    VkImageView depth_image_view;
    VkDeviceMemory depth_image_mem;

    // NOTE: semaphore and fence handling
    VkSemaphore default_semaphores[DefaultSemaphores_COUNT];

} VulkanContext;

typedef struct ApplicationContext
{
    HINSTANCE app_instance;
    HWND window_handle;
} ApplicationContext;

typedef union V2
{
    struct
    {
        f32 x;
        f32 y;
    };

    struct
    {
        f32 width;
        f32 height;
    };
} V2;

typedef union V2I
{
    struct
    {
        i32 x;
        i32 y;
    };

    struct
    {
        i32 width;
        i32 height;
    };
} V2I;

typedef union V2U
{
    struct
    {
        u32 x;
        u32 y;
    };

    struct
    {
        u32 width;
        u32 height;
    };
} V2U;

typedef union V4
{
    struct
    {
        f32 x;
        f32 y;
        f32 z;
        f32 w;
    };

    struct
    {
        f32 r;
        f32 g;
        f32 b;
        f32 a;
    };
} V4;

typedef struct Circle
{
    float x;
    float y;
    float radius;
    // NOTE opengl std140 layout compat(16-byte alignment)
    float pad;
} Circle;

typedef struct CircleList
{
    u32 count;
    // NOTE opengl std140 layout compat(16-byte alignment)
    u32 pad[3];
    Circle data[256];
} CircleList;

_Static_assert(offsetof(CircleList, data) == 16, "Incorrect Offset");

typedef struct tbs_ImageData
{
    u32 width;
    u32 height;
    u32 orig_channel_count;
    u32 channel_count;
    size_t size;
    unsigned char *data;
} tbs_ImageData;

typedef struct VertexAttribs
{
    V2 pos;
    V2 uv;
    V4 col;
} VertexAttribs;

_Static_assert(sizeof(VertexAttribs) == 32, "VertexAttribs unexpected size");

typedef struct tbsvk_Buffer
{
    VkBuffer buffer_handle;
    VkDeviceMemory memory_handle;
} tbsvk_Buffer;

typedef struct tbsvk_Image
{
    VkImage image_handle;
    VkDeviceMemory memory_handle;
    VkImageView image_view_handle;
} tbsvk_Image;

static VkCommandBufferBeginInfo default_cmd_begin_info =
{
    .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
    .flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
};

static VkFenceCreateInfo unsig_fence_ci =
{
    .sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
};

static VkFenceCreateInfo sig_fence_ci =
{
    .sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
    .flags = VK_FENCE_CREATE_SIGNALED_BIT,
};

static inline bool v2u_Equal(V2U a, V2U b)
{
   bool result = (a.x == b.x && a.y == b.y);
   return result;
}

static inline bool StringCompareZ(const char *s1, const char *s2)
{
    bool result = (strcmp(s1, s2) == 0);
    return result;
}

static inline void TBS_VK_CHECK(VkResult result)
{
    if(result != VK_SUCCESS)
    {
        assert(false);
    }
}

#define GetDefaultCmd(ctx, id) Internal_GetDefaultCmd(ctx, DefaultCmdType_##id)
static inline VkCommandBuffer Internal_GetDefaultCmd(VulkanContext *ctx, DefaultCmdType cmd_id)
{
    VkCommandBuffer result = ctx->default_cmd_buffers[cmd_id];
    return result;
}

#define GetDefaultSemaphore(ctx, id) Internal_GetDefaultSemaphore(ctx, DefaultSemaphores_##id)
static inline VkSemaphore Internal_GetDefaultSemaphore(VulkanContext *ctx, DefaultSemaphores sem_id)
{
    VkSemaphore result = ctx->default_semaphores[sem_id];
    return result;
}

// NOTE: These sizes are client area so does not include border size
// etc
static inline V2 win32_GetWindowSizeF32(HWND window_handle)
{
    RECT rect;
    GetClientRect(window_handle, &rect);
    V2 result = {.width = (f32)rect.right, .height = (f32)rect.bottom};
    return result;
}

static inline V2U win32_GetWindowSizeU32(HWND window_handle)
{
    RECT rect;
    GetClientRect(window_handle, &rect);
    V2U result = {.width = (u32)rect.right, .height = (u32)rect.bottom};
    return result;
}

static FileData debug_win32_ReadFile(const char *filename)
{
    FileData result = {0};
    HANDLE file_handle = CreateFile(filename, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL,NULL);
    assert(file_handle != INVALID_HANDLE_VALUE);

    LARGE_INTEGER tmp_file_size;
    unsigned long file_size = 0;
    if(GetFileSizeEx(file_handle, &tmp_file_size))
    {
        assert(tmp_file_size.QuadPart < LONG_MAX);
        file_size = (unsigned long)tmp_file_size.QuadPart;
    }
    else
    {
        assert(false);
    }

    unsigned long bytes_read = 0;
    char *buffer = malloc(file_size);
    if(ReadFile(file_handle, buffer, file_size, &bytes_read, NULL))
    {
        result.data = buffer;
        result.size = bytes_read;
    }
    else
    {
        assert(false);
    }

    assert(file_size == bytes_read);

    CloseHandle(file_handle);

    return result;
}

static void debug_win32_FreeFileData(FileData *file_data)
{
    assert(file_data->size > 0);
    assert(file_data->data);

    free(file_data->data);
    file_data->data = NULL;
    file_data->size = 0;
}

static LRESULT CALLBACK MainWindowCallbackProc(HWND window_handle, UINT msg, WPARAM wParam, LPARAM lParam)
{
    LRESULT result = 0;
    switch(msg)
    {
        case WM_DESTROY:
        {
            PostQuitMessage(0);
        } break;
        default:
        {
            result = DefWindowProc(window_handle, msg, wParam, lParam);
        }
    }

    return result;
}

static VKAPI_ATTR VkBool32 VKAPI_CALL VulkanDebugReportCallbackProc(VkDebugReportFlagsEXT flags,
                                                                    VkDebugReportObjectTypeEXT object_type,
                                                                    uint64_t object,
                                                                    size_t location,
                                                                    int32_t message_code,
                                                                    const char *layer_prefix,
                                                                    const char *message,
                                                                    void *user_data)
{
    printf("VULKAN_DEBUG_REPORT_MESSAGE::\n%s\n", message);
    return VK_FALSE;
}

static VkFence tbs_VulkanCreateFence(VkDevice device)
{
    VkFence result = VK_NULL_HANDLE;
    VkFenceCreateInfo ci = {.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO};
    TBS_VK_CHECK(vkCreateFence(device, &ci, NULL, &result));
    // TODO: handle errors
    return result;
}

// TODO: Parts of this is win32 specific
static bool InitBaseVulkan(HMODULE lib, HINSTANCE app_instance, HWND window_handle, VulkanContext *vulkan_ctx)
{
    bool result = false;
    vkGetInstanceProcAddr = (PFN_vkGetInstanceProcAddr)GetProcAddress(lib, "vkGetInstanceProcAddr");
    if(vkGetInstanceProcAddr)
    {
        printf("Successfully loaded function vkGetInstanceProcAddress\n");
        Vulkan_LoadGlobalFunctions();
        uint32_t version = 0;
        TBS_VK_CHECK(vkEnumerateInstanceVersion(&version));
        printf("Major version: %u\n", VK_API_VERSION_MAJOR(version));
        printf("Minor version: %u\n", VK_API_VERSION_MINOR(version));
        printf("Patch version: %u\n", VK_API_VERSION_PATCH(version));
        printf("Variant version: %u\n", VK_API_VERSION_VARIANT(version));

        u32 ext_count = 0;
        TBS_VK_CHECK(vkEnumerateInstanceExtensionProperties(NULL, &ext_count, NULL));
        printf("Extension count: %u\n", ext_count);
        assert(ext_count > 0);
        VkExtensionProperties *ext_properties = malloc(sizeof(VkExtensionProperties) * ext_count);
        TBS_VK_CHECK(vkEnumerateInstanceExtensionProperties(NULL, &ext_count, ext_properties));
        u32 prop_count = 0;
        TBS_VK_CHECK(vkEnumerateInstanceLayerProperties(&prop_count, NULL));
        assert(prop_count > 0);
        printf("\nProp count: %u\n", prop_count);
        VkLayerProperties *layer_properties = malloc(sizeof(VkLayerProperties) * prop_count);
        TBS_VK_CHECK(vkEnumerateInstanceLayerProperties(&prop_count, layer_properties));
        assert(prop_count > 0);

        const char *wanted_extensions[] =
        {
            VK_KHR_SURFACE_EXTENSION_NAME,
            VK_KHR_WIN32_SURFACE_EXTENSION_NAME,
            VK_EXT_DEBUG_REPORT_EXTENSION_NAME,
        };

        u32 match_count = 0;
        for(u32 a = 0; a < array_count(wanted_extensions); a++)
        {
            for(u32 b = 0; b < ext_count; b++)
            {
                if(StringCompareZ(wanted_extensions[a], ext_properties[b].extensionName))
                {
                    match_count++;
                    break;
                }
            }
        }

        assert(match_count == array_count(wanted_extensions));

        const char *wanted_layers[] =
        {
            "VK_LAYER_KHRONOS_validation",
        };

        match_count = 0;
        for(u32 a = 0; a < array_count(wanted_layers); a++)
        {
            for(u32 b = 0; b < prop_count; b++)
            {
                if(StringCompareZ(wanted_layers[a], layer_properties[b].layerName))
                {
                    match_count++;
                    break;
                }
            }
        }

        assert(match_count == array_count(wanted_layers));

        VkApplicationInfo app_info =
        {
            .sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
            .pApplicationName = "Kvase Engine",
            .applicationVersion = 1u,
            .apiVersion = version,
        };

        VkInstanceCreateInfo instance_ci =
        {
            .sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
            .pApplicationInfo = &app_info,
            .enabledLayerCount = array_count(wanted_layers),
            .ppEnabledLayerNames = wanted_layers,
            .enabledExtensionCount = array_count(wanted_extensions),
            .ppEnabledExtensionNames = wanted_extensions,
        };

        VkInstance instance_handle;
        TBS_VK_CHECK(vkCreateInstance(&instance_ci, NULL, &instance_handle));

        Vulkan_LoadInstanceFunctions(instance_handle);

        VkDebugReportCallbackCreateInfoEXT debug_callback_ci =
        {
            .sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CREATE_INFO_EXT,
            .flags = VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT | VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT,
            .pfnCallback = &VulkanDebugReportCallbackProc,
        };

        VkDebugReportCallbackEXT debug_callback;
        TBS_VK_CHECK(vkCreateDebugReportCallbackEXT(instance_handle, &debug_callback_ci, NULL, &debug_callback));

        VkWin32SurfaceCreateInfoKHR surface_ci =
        {
            .sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR,
            .hinstance = app_instance,
            .hwnd = window_handle,
        };

        VkSurfaceKHR surface;
        TBS_VK_CHECK(vkCreateWin32SurfaceKHR(instance_handle, &surface_ci, NULL, &surface));

        u32 device_count = 0;
        TBS_VK_CHECK(vkEnumeratePhysicalDevices(instance_handle, &device_count, NULL));
        assert(device_count > 0);
        VkPhysicalDevice *physical_devices = malloc(sizeof(VkPhysicalDevice) * device_count);
        TBS_VK_CHECK(vkEnumeratePhysicalDevices(instance_handle, &device_count, physical_devices));
        assert(device_count > 0);

        VkPhysicalDevice gpu_handle = NULL;
        u32 queue_family_index = 0;
        for(u32 di = 0; di < device_count; di++)
        {
            VkPhysicalDevice tmp_dev = physical_devices[di];
            u32 fam_count = 0;
            vkGetPhysicalDeviceQueueFamilyProperties(tmp_dev, &fam_count, NULL);
            assert(fam_count > 0);
            VkQueueFamilyProperties *fam_properties = malloc(sizeof(VkQueueFamilyProperties) * fam_count);
            vkGetPhysicalDeviceQueueFamilyProperties(tmp_dev, &fam_count, fam_properties);
            assert(fam_count > 0);
            for(u32 fi = 0; fi < fam_count; fi++)
            {
                printf("Queue Family Index: %u\n", fi);
                printf("Queue Count: %u\n", fam_properties[fi].queueCount);
                if(fam_properties[fi].queueFlags & (VK_QUEUE_GRAPHICS_BIT | VK_QUEUE_COMPUTE_BIT) && fam_properties[fi].queueCount > 1)
                {
                    VkBool32 support;
                    TBS_VK_CHECK(vkGetPhysicalDeviceSurfaceSupportKHR(tmp_dev, fi, surface, &support));
                    if(support)
                    {
                        queue_family_index = fi;
                        gpu_handle = tmp_dev;
                        break;
                    }
                }
            }

            free(fam_properties);
            if(gpu_handle)
            {
                break;
            }
        }

        assert(gpu_handle != NULL);
        printf("Selected Queue Family Index: %u\n", queue_family_index);


        f32 queue_priority = 1.0f;
        VkDeviceQueueCreateInfo device_queue_ci =
        {
            .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
            .queueFamilyIndex = queue_family_index,
            .queueCount = 2,
            .pQueuePriorities = &queue_priority,
        };

        const char *wanted_device_extensions[] =
        {
            VK_KHR_SWAPCHAIN_EXTENSION_NAME,
        };

        u32 device_ext_count;
        TBS_VK_CHECK(vkEnumerateDeviceExtensionProperties(gpu_handle, NULL, &device_ext_count, NULL));
        assert(device_ext_count > 0);
        VkExtensionProperties *device_extensions = malloc(sizeof(VkExtensionProperties) * device_ext_count);
        TBS_VK_CHECK(vkEnumerateDeviceExtensionProperties(gpu_handle, NULL, &device_ext_count, device_extensions));
        assert(device_ext_count > 0);

        match_count = 0;
        for(u32 a = 0; a < array_count(wanted_device_extensions); a++)
        {
            for(u32 b = 0; b < device_ext_count; b++)
            {
                if(StringCompareZ(wanted_device_extensions[a], device_extensions[b].extensionName))
                {
                    match_count++;
                }
            }
        }

        assert(match_count == array_count(wanted_device_extensions));

        VkDeviceCreateInfo device_ci =
        {
            .sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
            .queueCreateInfoCount = 1,
            .pQueueCreateInfos = &device_queue_ci,
            .enabledExtensionCount = array_count(wanted_device_extensions),
            .ppEnabledExtensionNames = wanted_device_extensions,
            .pEnabledFeatures = NULL,
        };

        VkDevice device_handle;
        TBS_VK_CHECK(vkCreateDevice(gpu_handle, &device_ci, NULL, &device_handle));

        Vulkan_LoadDeviceFunctions(device_handle);

        VkQueue graphics_queue;
        VkQueue compute_queue;
        vkGetDeviceQueue(device_handle, queue_family_index, 0, &graphics_queue);
        vkGetDeviceQueue(device_handle, queue_family_index, 1, &compute_queue);

        vulkan_ctx->instance_handle = instance_handle;
        vulkan_ctx->surface_handle = surface;
        vulkan_ctx->gpu_handle = gpu_handle;
        vulkan_ctx->device_handle = device_handle;
        vulkan_ctx->graphics_queue_handle = graphics_queue;
        vulkan_ctx->compute_queue_handle = compute_queue;
        vulkan_ctx->queue_family_index = queue_family_index;

        vkGetPhysicalDeviceMemoryProperties(gpu_handle, &vulkan_ctx->gpu_mem_props);

        result = true;
    }

    return result;
}

static bool CreateAndInitSwapchain(ApplicationContext *app_ctx, VulkanContext *vulkan_ctx)
{
    VkSurfaceCapabilitiesKHR surface_capabilities;
    TBS_VK_CHECK(vkGetPhysicalDeviceSurfaceCapabilitiesKHR(vulkan_ctx->gpu_handle, vulkan_ctx->surface_handle, &surface_capabilities));

    assert(surface_capabilities.supportedTransforms & VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR);
    assert(surface_capabilities.currentTransform == VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR);
    assert(surface_capabilities.maxImageCount >= 2 || surface_capabilities.maxImageCount == 0);
    assert(surface_capabilities.currentExtent.width > 0 || surface_capabilities.currentExtent.height > 0);
    printf("Current extent: %u, %u\n", surface_capabilities.currentExtent.width, surface_capabilities.currentExtent.height);
    printf("Min extent: %u, %u\n", surface_capabilities.minImageExtent.width, surface_capabilities.minImageExtent.height);
    printf("Max extent: %u, %u\n", surface_capabilities.maxImageExtent.width, surface_capabilities.maxImageExtent.height);


    V2U window_size = win32_GetWindowSizeU32(app_ctx->window_handle);
    VkExtent2D wanted_extent = {.width = window_size.width, .height = window_size.height};

    // NOTE: Handle when window is minimized
    assert(wanted_extent.width > 0 && wanted_extent.height > 0);
    // TODO: Just for testing
    assert(surface_capabilities.currentExtent.width == window_size.width &&
           surface_capabilities.currentExtent.height == window_size.height);
    if(surface_capabilities.currentExtent.width != 0xffffffff)
    {
        wanted_extent = surface_capabilities.currentExtent;
    }

    u32 format_count = 0;
    TBS_VK_CHECK(vkGetPhysicalDeviceSurfaceFormatsKHR(vulkan_ctx->gpu_handle, vulkan_ctx->surface_handle, &format_count, NULL));
    assert(format_count > 0);
    VkSurfaceFormatKHR *surface_formats = malloc(sizeof(VkSurfaceFormatKHR) * format_count);
    TBS_VK_CHECK(vkGetPhysicalDeviceSurfaceFormatsKHR(vulkan_ctx->gpu_handle, vulkan_ctx->surface_handle, &format_count, surface_formats));
    assert(format_count > 0);

    u32 format_index = UINT32_MAX;
    for(u32 i = 0; i < format_count; i++)
    {
        if(surface_formats[i].format == VK_FORMAT_B8G8R8A8_UNORM && surface_formats[i].colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
        {
            format_index = i;
        }
    }

    // TODO: handle error bad format
    assert(format_index != UINT32_MAX);

    VkSwapchainCreateInfoKHR swapchain_ci =
    {
        .sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
        .surface = vulkan_ctx->surface_handle,
        .minImageCount = 2,
        .imageFormat = surface_formats[format_index].format,
        .imageColorSpace = surface_formats[format_index].colorSpace,
        .imageExtent = wanted_extent,
        .imageArrayLayers = 1,
        .imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
        .imageSharingMode = VK_SHARING_MODE_EXCLUSIVE,
        .preTransform = surface_capabilities.currentTransform,
        .compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
        // TODO: Test different modes here to see differences
        // We then need to check which are supported by
        // calling vkGetPhysicalDeviceSurfacePresentModesKHR
        .presentMode = VK_PRESENT_MODE_FIFO_KHR,
        .clipped = VK_TRUE,
        .oldSwapchain = VK_NULL_HANDLE,
    };

    VkSwapchainKHR swapchain_handle;
    TBS_VK_CHECK(vkCreateSwapchainKHR(vulkan_ctx->device_handle, &swapchain_ci, NULL, &swapchain_handle));

    u32 image_count = 0;
    TBS_VK_CHECK(vkGetSwapchainImagesKHR(vulkan_ctx->device_handle, swapchain_handle, &image_count, NULL));
    assert(image_count >= 2);
    VkImage *swapchain_images = malloc(sizeof(VkImage) * image_count);
    TBS_VK_CHECK(vkGetSwapchainImagesKHR(vulkan_ctx->device_handle, swapchain_handle, &image_count, swapchain_images));
    assert(image_count >= 2);

    VkImageView *swapchain_image_views = malloc(sizeof(VkImageView) * image_count);

    VkFenceCreateInfo submit_fence_ci = {.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO};
    VkFence submit_fence;
    TBS_VK_CHECK(vkCreateFence(vulkan_ctx->device_handle, &submit_fence_ci, NULL, &submit_fence));

    VkCommandBuffer setup_cmd = GetDefaultCmd(vulkan_ctx, SwapchainSetup);
    VkCommandBufferBeginInfo setup_begin_info = {.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO, .flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT};

    for(size_t pi = 0; pi < image_count; pi++)
    {

        TBS_VK_CHECK(vkBeginCommandBuffer(setup_cmd, &setup_begin_info));

        VkImageMemoryBarrier image_init_barr =
        {
            .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
            .srcAccessMask = 0,
            .dstAccessMask = VK_ACCESS_MEMORY_READ_BIT,
            .oldLayout = VK_IMAGE_LAYOUT_UNDEFINED,
            .newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
            .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
            .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
            .image = swapchain_images[pi],
            .subresourceRange =
            {
                .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                .baseMipLevel = 0,
                .levelCount = 1,
                .baseArrayLayer = 0,
                .layerCount = 1,
            },
        };

        vkCmdPipelineBarrier(setup_cmd,
                             VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
                             VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
                             0,
                             0, NULL,
                             0, NULL,
                             1, &image_init_barr);

        TBS_VK_CHECK(vkEndCommandBuffer(setup_cmd));

        VkPipelineStageFlags wait_stage_mask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        VkSubmitInfo submit_info =
        {
            .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
            .pWaitDstStageMask = &wait_stage_mask,
            .commandBufferCount = 1,
            .pCommandBuffers = &setup_cmd,
        };
        TBS_VK_CHECK(vkQueueSubmit(vulkan_ctx->graphics_queue_handle, 1, &submit_info, submit_fence));

        TBS_VK_CHECK(vkWaitForFences(vulkan_ctx->device_handle, 1, &submit_fence, VK_TRUE, UINT64_MAX));
        TBS_VK_CHECK(vkResetFences(vulkan_ctx->device_handle, 1, &submit_fence));

        TBS_VK_CHECK(vkResetCommandBuffer(setup_cmd, 0));

        VkImageViewCreateInfo image_view_ci =
        {
            .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
            .image = swapchain_images[pi],
            .viewType = VK_IMAGE_VIEW_TYPE_2D,
            .format = surface_formats[0].format,
            .components =
            {
                VK_COMPONENT_SWIZZLE_IDENTITY,
                VK_COMPONENT_SWIZZLE_IDENTITY,
                VK_COMPONENT_SWIZZLE_IDENTITY,
                VK_COMPONENT_SWIZZLE_IDENTITY,
            },
            .subresourceRange =
            {
                .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                .baseMipLevel = 0,
                .levelCount = 1,
                .baseArrayLayer = 0,
                .layerCount = 1,
            },
        };

        TBS_VK_CHECK(vkCreateImageView(vulkan_ctx->device_handle, &image_view_ci, NULL, &swapchain_image_views[pi]));
    }

    vkDestroyFence(vulkan_ctx->device_handle, submit_fence, NULL);

    vulkan_ctx->surface_formats = surface_formats;
    vulkan_ctx->surface_format_count = format_count;
    vulkan_ctx->swapchain_images = swapchain_images;
    vulkan_ctx->swapchain_image_count = image_count;
    vulkan_ctx->swapchain_image_views = swapchain_image_views;
    vulkan_ctx->swapchain_extent = wanted_extent;
    vulkan_ctx->swapchain_handle = swapchain_handle;

    // TODO: handle errors
    return true;
}

static bool CreateAndInitDepthImageView(VulkanContext *vulkan_ctx, VkFormat depth_format)
{
    VkImageCreateInfo depth_image_ci =
    {
        .sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
        .imageType = VK_IMAGE_TYPE_2D,
        // TODO(torgrim): Check that the surface actually support this format
        .format = depth_format,
        .extent = {.width = vulkan_ctx->swapchain_extent.width, .height = vulkan_ctx->swapchain_extent.height, .depth = 1},
        .mipLevels = 1,
        .arrayLayers = 1,
        .samples = VK_SAMPLE_COUNT_1_BIT,
        .tiling = VK_IMAGE_TILING_OPTIMAL,
        .usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
    };

    VkImage depth_image;
    TBS_VK_CHECK(vkCreateImage(vulkan_ctx->device_handle, &depth_image_ci, NULL, &depth_image));

    VkMemoryRequirements mem_req;
    vkGetImageMemoryRequirements(vulkan_ctx->device_handle, depth_image, &mem_req);

    VkMemoryPropertyFlags wanted_memory = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;

    _Static_assert(VK_MAX_MEMORY_TYPES == 32u, "VK_MAX_MEMORY_TYPES invalid value");
    u32 mem_index = UINT32_MAX;
    for(u32 mi = 0; mi < vulkan_ctx->gpu_mem_props.memoryTypeCount; mi++)
    {
        if(mem_req.memoryTypeBits & (1 << mi) && vulkan_ctx->gpu_mem_props.memoryTypes[mi].propertyFlags & wanted_memory)
        {
            mem_index = mi;
            break;
        }
    }

    assert(mem_index != UINT32_MAX);

    VkMemoryAllocateInfo mem_allocate_info =
    {
        .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
        .allocationSize = mem_req.size,
        .memoryTypeIndex = mem_index,
    };

    VkDeviceMemory image_memory;
    TBS_VK_CHECK(vkAllocateMemory(vulkan_ctx->device_handle, &mem_allocate_info, NULL, &image_memory));
    TBS_VK_CHECK(vkBindImageMemory(vulkan_ctx->device_handle, depth_image, image_memory, 0));


    VkFenceCreateInfo depth_submit_fence_ci = {.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO};
    VkFence depth_submit_fence;
    TBS_VK_CHECK(vkCreateFence(vulkan_ctx->device_handle, &depth_submit_fence_ci, NULL, &depth_submit_fence));

    VkCommandBufferBeginInfo cmd_begin_info =
    {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        .flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
    };

    VkCommandBuffer cmd_buffer = GetDefaultCmd(vulkan_ctx, DepthSetup);
    TBS_VK_CHECK(vkBeginCommandBuffer(cmd_buffer, &cmd_begin_info));

    VkImageMemoryBarrier depth_trans_barrier =
    {
        .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
        .srcAccessMask = 0,
        .dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT,
        .oldLayout = VK_IMAGE_LAYOUT_UNDEFINED,
        .newLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
        .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .image = depth_image,
        .subresourceRange =
        {
            .aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT,
            .baseMipLevel = 0,
            .levelCount = 1,
            .baseArrayLayer = 0,
            .layerCount = 1,
        },
    };

    vkCmdPipelineBarrier(cmd_buffer,
                         VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
                         VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT | VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT,
                         0,
                         0, NULL,
                         0, NULL,
                         1, &depth_trans_barrier
                         );

    TBS_VK_CHECK(vkEndCommandBuffer(cmd_buffer));

    VkSubmitInfo submit_info =
    {
        .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
        .waitSemaphoreCount = 0,
        .pWaitSemaphores = NULL,
        .pWaitDstStageMask = NULL,
        .commandBufferCount = 1,
        .pCommandBuffers = &cmd_buffer,
        .signalSemaphoreCount = 0,
        .pSignalSemaphores = NULL,
    };

    TBS_VK_CHECK(vkQueueSubmit(vulkan_ctx->graphics_queue_handle, 1, &submit_info, depth_submit_fence));

    TBS_VK_CHECK(vkWaitForFences(vulkan_ctx->device_handle, 1, &depth_submit_fence, VK_TRUE, UINT64_MAX));
    TBS_VK_CHECK(vkResetFences(vulkan_ctx->device_handle, 1, &depth_submit_fence));

    TBS_VK_CHECK(vkResetCommandBuffer(cmd_buffer, 0));

    VkImageViewCreateInfo depth_view_ci =
    {
        .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
        .image = depth_image,
        .viewType = VK_IMAGE_VIEW_TYPE_2D,
        .format = depth_format,
        .components =
        {
            VK_COMPONENT_SWIZZLE_IDENTITY,
            VK_COMPONENT_SWIZZLE_IDENTITY,
            VK_COMPONENT_SWIZZLE_IDENTITY,
            VK_COMPONENT_SWIZZLE_IDENTITY,
        },
        .subresourceRange =
        {
            .aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT,
            .baseMipLevel = 0,
            .levelCount = 1,
            .baseArrayLayer = 0,
            .layerCount = 1,
        },
    };

    VkImageView depth_image_view;
    TBS_VK_CHECK(vkCreateImageView(vulkan_ctx->device_handle, &depth_view_ci, NULL, &depth_image_view));

    vkDestroyFence(vulkan_ctx->device_handle, depth_submit_fence, NULL);

    vulkan_ctx->depth_image = depth_image;
    vulkan_ctx->depth_image_view = depth_image_view;
    vulkan_ctx->depth_image_mem = image_memory;

    // TODO: handle errors
    return true;
}

static bool CreateDefaultGraphicsPipeline(VulkanContext *vulkan_ctx,
                                          VkShaderModule vert_shader_module, VkShaderModule frag_shader_module,
                                          VkDescriptorSetLayout *desc_layouts, u32 desc_layout_count)
{
    assert(vulkan_ctx->graphics_pipeline_layout_handle == VK_NULL_HANDLE);
    assert(vulkan_ctx->graphics_pipeline_handle == VK_NULL_HANDLE);

    VkPipelineShaderStageCreateInfo stages[] =
    {
        {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
            .stage = VK_SHADER_STAGE_VERTEX_BIT,
            .module = vert_shader_module,
            .pName = "main",
        },
        {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
            .stage = VK_SHADER_STAGE_FRAGMENT_BIT,
            .module = frag_shader_module,
            .pName = "main",
        },
    };

    VkVertexInputBindingDescription vertex_binding_list[] =
    {
        {
            .binding = 0,
            .stride = sizeof(VertexAttribs),
            .inputRate = VK_VERTEX_INPUT_RATE_VERTEX,
        },
    };

    VkVertexInputAttributeDescription vertex_attrib_list[] =
    {
        {
            .location = 0,
            .binding = 0,
            .format = VK_FORMAT_R32G32_SFLOAT,
        },
        {
            .location = 1,
            .binding = 0,
            .format = VK_FORMAT_R32G32_SFLOAT,
            .offset = offsetof(VertexAttribs, uv),
        },
        {
            .location = 2,
            .binding = 0,
            .format = VK_FORMAT_R32G32B32A32_SFLOAT,
            .offset = offsetof(VertexAttribs, col),
        },
    };

    VkPipelineVertexInputStateCreateInfo vert_input_state_ci =
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
        .vertexBindingDescriptionCount = array_count(vertex_binding_list),
        .pVertexBindingDescriptions = vertex_binding_list,
        .vertexAttributeDescriptionCount = array_count(vertex_attrib_list),
        .pVertexAttributeDescriptions = vertex_attrib_list,
    };

    VkPipelineInputAssemblyStateCreateInfo input_assembly_state_ci =
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
        .topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
        .primitiveRestartEnable = VK_FALSE,
    };

    VkPipelineViewportStateCreateInfo viewport_state_ci =
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
        .viewportCount = 1,
        .scissorCount = 1,
    };

    VkPipelineRasterizationStateCreateInfo rasterization_state_ci =
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
        .depthClampEnable = VK_FALSE,
        .rasterizerDiscardEnable = VK_FALSE,
        .polygonMode = VK_POLYGON_MODE_FILL,
        .cullMode = VK_CULL_MODE_NONE,
        .frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE,
        .depthBiasEnable = VK_FALSE,
        .lineWidth = 1.0f,
    };

    VkPipelineMultisampleStateCreateInfo multisample_state_ci =
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
        .rasterizationSamples = VK_SAMPLE_COUNT_1_BIT,
    };

    VkPipelineDepthStencilStateCreateInfo depth_stencil_state_ci =
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
        .depthTestEnable = VK_FALSE,
        .depthWriteEnable = VK_FALSE,
        .depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL,
    };

    VkPipelineColorBlendAttachmentState color_blend_attachment =
    {
        .blendEnable = VK_TRUE,
        .srcColorBlendFactor = VK_BLEND_FACTOR_SRC_COLOR,
        .dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_DST_COLOR,
        .colorBlendOp = VK_BLEND_OP_ADD,
        .srcAlphaBlendFactor = VK_BLEND_FACTOR_ZERO,
        .dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO,
        .alphaBlendOp = VK_BLEND_OP_ADD,
        .colorWriteMask = 0xf,
    };

    VkPipelineColorBlendStateCreateInfo color_blend_state_ci =
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
        .logicOpEnable = VK_FALSE,
        .logicOp = VK_LOGIC_OP_CLEAR,
        .attachmentCount = 1,
        .pAttachments = &color_blend_attachment,
        .blendConstants = {0.0f, 0.0f, 0.0f, 0.0f},
    };

    VkDynamicState dynamic_state_list[] =
    {
        VK_DYNAMIC_STATE_VIEWPORT,
        VK_DYNAMIC_STATE_SCISSOR,
    };

    VkPipelineDynamicStateCreateInfo dynamic_state_ci =
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO,
        .dynamicStateCount = array_count(dynamic_state_list),
        .pDynamicStates = dynamic_state_list,
    };


    VkPipelineLayoutCreateInfo pipeline_layout_ci =
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
        .setLayoutCount = desc_layout_count,
        .pSetLayouts = desc_layouts,
    };

    VkPipelineLayout pipeline_layout_handle;
    TBS_VK_CHECK(vkCreatePipelineLayout(vulkan_ctx->device_handle, &pipeline_layout_ci, NULL, &pipeline_layout_handle));

    VkGraphicsPipelineCreateInfo graphics_pipeline_ci =
    {
        .sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
        .stageCount = array_count(stages),
        .pStages = stages,
        .pVertexInputState = &vert_input_state_ci,
        .pInputAssemblyState = &input_assembly_state_ci,
        .pViewportState = &viewport_state_ci,
        .pRasterizationState = &rasterization_state_ci,
        .pMultisampleState = &multisample_state_ci,
        .pDepthStencilState = &depth_stencil_state_ci,
        .pColorBlendState = &color_blend_state_ci,
        .pDynamicState = &dynamic_state_ci,
        .layout = pipeline_layout_handle,
        .renderPass = vulkan_ctx->default_render_pass_handle,
    };

    VkPipeline graphics_pipeline_handle;
    TBS_VK_CHECK(vkCreateGraphicsPipelines(vulkan_ctx->device_handle, VK_NULL_HANDLE, 1, &graphics_pipeline_ci, NULL, &graphics_pipeline_handle));

    vulkan_ctx->graphics_pipeline_layout_handle = pipeline_layout_handle;
    vulkan_ctx->graphics_pipeline_handle = graphics_pipeline_handle;

    // TODO: Handle errors
    return true;
}

static bool CreateDefaultComputePipeline(VulkanContext *vulkan_ctx, VkShaderModule compute_shader_module, VkDescriptorSetLayout *desc_set_layouts, u32 layout_count)
{
    assert(vulkan_ctx->compute_pipeline_layout_handle == VK_NULL_HANDLE);
    assert(vulkan_ctx->compute_pipeline_handle == VK_NULL_HANDLE);

    VkPipelineShaderStageCreateInfo stage =
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
        .stage = VK_SHADER_STAGE_COMPUTE_BIT,
        .module = compute_shader_module,
        .pName = "main",
    };

    VkPipelineLayoutCreateInfo layout_ci =
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
        .setLayoutCount = layout_count,
        .pSetLayouts = desc_set_layouts,
    };

    VkPipelineLayout pipeline_layout;
    TBS_VK_CHECK(vkCreatePipelineLayout(vulkan_ctx->device_handle, &layout_ci, NULL, &pipeline_layout));

    VkComputePipelineCreateInfo pipeline_ci =
    {
        .sType = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO,
        .stage = stage,
        .layout = pipeline_layout,
    };

    VkPipeline compute_pipeline;
    TBS_VK_CHECK(vkCreateComputePipelines(vulkan_ctx->device_handle, VK_NULL_HANDLE, 1, &pipeline_ci, NULL, &compute_pipeline));

    vulkan_ctx->compute_pipeline_handle = compute_pipeline;
    vulkan_ctx->compute_pipeline_layout_handle = pipeline_layout;

    // TODO: handle errors
    return true;
}

static bool CreateDefaultRenderPass(VulkanContext *vulkan_ctx, VkFormat depth_format)
{
    VkAttachmentDescription attachment_list[] =
    {
        {
            .format = vulkan_ctx->surface_formats[vulkan_ctx->swapchain_image_format_index].format,
            .samples = VK_SAMPLE_COUNT_1_BIT,
            .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
            .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
            .initialLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
            .finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
        },
#if 0
        {
            .format = depth_format,
            .samples = VK_SAMPLE_COUNT_1_BIT,
            .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
            .storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
            .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
            .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
            .initialLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
            .finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
        },
#endif

    };

    VkAttachmentReference color_attachment_ref = {.attachment = 0, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL};
    //VkAttachmentReference depth_attachment_ref = {.attachment = 1, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL};
    VkSubpassDescription subpass =
    {
        .colorAttachmentCount = 1,
        .pColorAttachments = &color_attachment_ref,
        //.pDepthStencilAttachment = &depth_attachment_ref,
    };

    VkRenderPassCreateInfo render_pass_ci =
    {
        .sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
        .attachmentCount = array_count(attachment_list),
        .pAttachments = attachment_list,
        .subpassCount = 1,
        .pSubpasses = &subpass,
        .dependencyCount = 0,
        .pDependencies = NULL,
    };

    VkRenderPass render_pass_handle;
    vkCreateRenderPass(vulkan_ctx->device_handle, &render_pass_ci, NULL, &render_pass_handle);
    vulkan_ctx->default_render_pass_handle = render_pass_handle;

    // TODO: handle errors
    return true;
}

static bool CreateDefaultFramebuffers(VulkanContext *vulkan_ctx)
{
    assert(vulkan_ctx->framebuffers == NULL);
    assert(vulkan_ctx->framebuffer_count == 0);

    VkFramebuffer *framebuffers = malloc(sizeof(VkFramebuffer) * vulkan_ctx->swapchain_image_count);
    for(size_t fi = 0; fi < vulkan_ctx->swapchain_image_count; fi++)
    {
        VkImageView fb_attachments[] =
        {
            vulkan_ctx->swapchain_image_views[fi],
            //vulkan_ctx->depth_image_view,
        };
        VkFramebufferCreateInfo framebuffer_ci =
        {
            .sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
            .renderPass = vulkan_ctx->default_render_pass_handle,
            .attachmentCount = array_count(fb_attachments),
            .pAttachments = fb_attachments,
            .width = vulkan_ctx->swapchain_extent.width,
            .height = vulkan_ctx->swapchain_extent.height,
            .layers = 1,
        };

        vkCreateFramebuffer(vulkan_ctx->device_handle, &framebuffer_ci, NULL, &framebuffers[fi]);
    }

    vulkan_ctx->framebuffers = framebuffers;
    vulkan_ctx->framebuffer_count = vulkan_ctx->swapchain_image_count;

    // TODO: handle errors
    return true;
}

static void DestroySwapchainResources(VulkanContext *vulkan_ctx)
{
    vkDeviceWaitIdle(vulkan_ctx->device_handle);
    if(vulkan_ctx->graphics_pipeline_layout_handle != VK_NULL_HANDLE)
    {
        vkDestroyPipelineLayout(vulkan_ctx->device_handle, vulkan_ctx->graphics_pipeline_layout_handle, NULL);
        vulkan_ctx->graphics_pipeline_layout_handle = VK_NULL_HANDLE;
    }
    if(vulkan_ctx->graphics_pipeline_handle != VK_NULL_HANDLE)
    {
        vkDestroyPipeline(vulkan_ctx->device_handle, vulkan_ctx->graphics_pipeline_handle, NULL);
        vulkan_ctx->graphics_pipeline_handle = VK_NULL_HANDLE;
    }

    if(vulkan_ctx->framebuffers != NULL)
    {
        for(u32 i = 0; i < vulkan_ctx->framebuffer_count; i++)
        {
            vkDestroyFramebuffer(vulkan_ctx->device_handle, vulkan_ctx->framebuffers[i], NULL);
            vulkan_ctx->framebuffers[i] = VK_NULL_HANDLE;
        }

        free(vulkan_ctx->framebuffers);
        vulkan_ctx->framebuffers = 0;
        vulkan_ctx->framebuffer_count = 0;
    }

    // TODO(torgrim): Handle images better
    if(vulkan_ctx->depth_image_view != VK_NULL_HANDLE)
    {
        assert(vulkan_ctx->depth_image != VK_NULL_HANDLE);
        assert(vulkan_ctx->depth_image_mem != VK_NULL_HANDLE);
        vkDestroyImageView(vulkan_ctx->device_handle, vulkan_ctx->depth_image_view, NULL);
        vkFreeMemory(vulkan_ctx->device_handle, vulkan_ctx->depth_image_mem, NULL);
        vkDestroyImage(vulkan_ctx->device_handle, vulkan_ctx->depth_image, NULL);
    }

    if(vulkan_ctx->swapchain_handle != VK_NULL_HANDLE)
    {
        assert(vulkan_ctx->swapchain_image_count > 0);
        for(u32 i = 0; i < vulkan_ctx->swapchain_image_count; i++)
        {
            vkDestroyImageView(vulkan_ctx->device_handle, vulkan_ctx->swapchain_image_views[i], NULL);
        }

        vkDestroySwapchainKHR(vulkan_ctx->device_handle, vulkan_ctx->swapchain_handle, NULL);

        free(vulkan_ctx->swapchain_images);
        free(vulkan_ctx->swapchain_image_views);

        vulkan_ctx->swapchain_images = NULL;
        vulkan_ctx->swapchain_image_views = NULL;
        vulkan_ctx->swapchain_image_count = 0;
    }

    if(vulkan_ctx->surface_format_count > 0)
    {
        free(vulkan_ctx->surface_formats);
        vulkan_ctx->surface_formats = NULL;
        vulkan_ctx->surface_format_count = 0;
    }
}

static VkDeviceMemory tbsvk_AllocateAndBindBufferMemory(VulkanContext *vulkan_ctx, VkBuffer buffer_handle, VkMemoryPropertyFlags wanted_mem_type)
{
    VkMemoryRequirements buffer_mem_req;
    vkGetBufferMemoryRequirements(vulkan_ctx->device_handle, buffer_handle, &buffer_mem_req);

    _Static_assert(VK_MAX_MEMORY_TYPES == 32u, "VK_MAX_MEMORY_TYPES invalid value");
    u32 mem_index = UINT32_MAX;
    for(u32 mi = 0; mi < vulkan_ctx->gpu_mem_props.memoryTypeCount; mi++)
    {
        if(buffer_mem_req.memoryTypeBits & (1 << mi) && vulkan_ctx->gpu_mem_props.memoryTypes[mi].propertyFlags & wanted_mem_type)
        {
            mem_index = mi;
            break;
        }
    }

    assert(mem_index != UINT32_MAX);

    VkMemoryAllocateInfo alloc_info =
    {
        .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
        .allocationSize = buffer_mem_req.size,
        .memoryTypeIndex = mem_index,
    };

    VkDeviceMemory buffer_memory;
    TBS_VK_CHECK(vkAllocateMemory(vulkan_ctx->device_handle, &alloc_info, NULL, &buffer_memory));
    TBS_VK_CHECK(vkBindBufferMemory(vulkan_ctx->device_handle, buffer_handle, buffer_memory, 0));

    return buffer_memory;
}

static VkDeviceMemory tbsvk_AllocateAndBindImageMemory(VulkanContext *vulkan_ctx, VkImage image_handle, VkMemoryPropertyFlags wanted_mem_type)
{
    VkMemoryRequirements image_mem_req;
    vkGetImageMemoryRequirements(vulkan_ctx->device_handle, image_handle, &image_mem_req);

    _Static_assert(VK_MAX_MEMORY_TYPES == 32u, "VK_MAX_MEMORY_TYPES invalid value");
    u32 mem_index = UINT32_MAX;
    for(u32 mi = 0; mi < vulkan_ctx->gpu_mem_props.memoryTypeCount; mi++)
    {
        if(image_mem_req.memoryTypeBits & (1 << mi) && vulkan_ctx->gpu_mem_props.memoryTypes[mi].propertyFlags & wanted_mem_type)
        {
            mem_index = mi;
            break;
        }
    }

    assert(mem_index != UINT32_MAX);

    VkMemoryAllocateInfo alloc_info =
    {
        .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
        .allocationSize = image_mem_req.size,
        .memoryTypeIndex = mem_index,
    };

    VkDeviceMemory image_memory;
    TBS_VK_CHECK(vkAllocateMemory(vulkan_ctx->device_handle, &alloc_info, NULL, &image_memory));
    TBS_VK_CHECK(vkBindImageMemory(vulkan_ctx->device_handle, image_handle, image_memory, 0));

    return image_memory;
}

// TODO: For now this is host visible memory
static tbsvk_Buffer CreateStorageBufferDefault(VulkanContext *vulkan_ctx, VkDeviceSize buffer_byte_size)
{
    assert(buffer_byte_size > 0);

    VkBufferCreateInfo buffer_ci =
    {
        .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .size = buffer_byte_size,
        .usage = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
    };

    VkBuffer buffer_handle;
    TBS_VK_CHECK(vkCreateBuffer(vulkan_ctx->device_handle, &buffer_ci, NULL, &buffer_handle));

    VkDeviceMemory buffer_mem = tbsvk_AllocateAndBindBufferMemory(vulkan_ctx, buffer_handle, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);

    tbsvk_Buffer result = {.buffer_handle = buffer_handle, .memory_handle = buffer_mem};

    return result;
}

static tbsvk_Image CreateImageTargetDefault2D(VulkanContext *vulkan_ctx)
{
    VkImageCreateInfo image_ci =
    {
        .sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
        .imageType = VK_IMAGE_TYPE_2D,
        .format = VK_FORMAT_R8G8B8A8_UNORM,
        .extent =
        {
            .width = vulkan_ctx->swapchain_extent.width,
            .height = vulkan_ctx->swapchain_extent.height,
            .depth = 1,
        },
        .mipLevels = 1,
        .arrayLayers = 1,
        .samples = VK_SAMPLE_COUNT_1_BIT,
        .tiling = VK_IMAGE_TILING_OPTIMAL,
        .usage = VK_IMAGE_USAGE_STORAGE_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
    };

    VkImage image_handle;
    TBS_VK_CHECK(vkCreateImage(vulkan_ctx->device_handle, &image_ci, NULL, &image_handle));

    VkDeviceMemory image_mem = tbsvk_AllocateAndBindImageMemory(vulkan_ctx, image_handle, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

    VkImageViewCreateInfo image_view_ci =
    {
        .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
        .image = image_handle,
        .viewType = VK_IMAGE_VIEW_TYPE_2D,
        .format = image_ci.format,
        .components =
        {
            .r = VK_COMPONENT_SWIZZLE_IDENTITY,
            .g = VK_COMPONENT_SWIZZLE_IDENTITY,
            .b = VK_COMPONENT_SWIZZLE_IDENTITY,
            .a = VK_COMPONENT_SWIZZLE_IDENTITY,
        },
        .subresourceRange =
        {
            .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
            .baseMipLevel = 0,
            .levelCount = 1,
            .baseArrayLayer = 0,
            .layerCount = 1,
        },
    };

    VkImageView image_view_handle;
    TBS_VK_CHECK(vkCreateImageView(vulkan_ctx->device_handle, &image_view_ci, NULL, &image_view_handle));

    VkImageMemoryBarrier from_undef_to_shader_readwrite_barr =
    {
        .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
        .srcAccessMask = 0,
        .dstAccessMask = VK_ACCESS_SHADER_READ_BIT | VK_ACCESS_SHADER_WRITE_BIT,
        .oldLayout = VK_IMAGE_LAYOUT_UNDEFINED,
        .newLayout = VK_IMAGE_LAYOUT_GENERAL,
        .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .image = image_handle,
        .subresourceRange =
        {
            .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
            .baseMipLevel = 0,
            .levelCount = 1,
            .baseArrayLayer = 0,
            .layerCount = 1,
        },
    };

    VkCommandBufferBeginInfo cmd_begin_info =
    {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        .flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
    };

    VkCommandBuffer image_layout_cmd = GetDefaultCmd(vulkan_ctx, ImageLayout);
    TBS_VK_CHECK(vkBeginCommandBuffer(image_layout_cmd, &cmd_begin_info));

    vkCmdPipelineBarrier(image_layout_cmd,
                         VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT,
                         0,
                         0, NULL,
                         0, NULL,
                         1, &from_undef_to_shader_readwrite_barr);

    TBS_VK_CHECK(vkEndCommandBuffer(image_layout_cmd));

    VkSubmitInfo submit_info =
    {
        .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
        .commandBufferCount = 1,
        .pCommandBuffers = &image_layout_cmd,
    };

    VkFence fence = tbs_VulkanCreateFence(vulkan_ctx->device_handle);
    TBS_VK_CHECK(vkQueueSubmit(vulkan_ctx->graphics_queue_handle, 1, &submit_info, fence));

    TBS_VK_CHECK(vkWaitForFences(vulkan_ctx->device_handle, 1, &fence, VK_TRUE, UINT64_MAX));
    TBS_VK_CHECK(vkResetFences(vulkan_ctx->device_handle, 1, &fence));
    vkDestroyFence(vulkan_ctx->device_handle, fence, NULL);

    tbsvk_Image result =
    {
        .image_handle = image_handle,
        .image_view_handle = image_view_handle,
        .memory_handle = image_mem,
    };

    return result;
}

static tbsvk_Image CreateTextureDefault2D(VulkanContext *vulkan_ctx, tbs_ImageData *image_data)
{
    VkImageCreateInfo image_ci =
    {
        .sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
        .imageType = VK_IMAGE_TYPE_2D,
        .format = VK_FORMAT_R8G8B8A8_UNORM,
        .extent =
        {
            .width = image_data->width,
            .height = image_data->height,
            .depth = 1,
        },
        .mipLevels = 1,
        .arrayLayers = 1,
        .samples = VK_SAMPLE_COUNT_1_BIT,
        .tiling = VK_IMAGE_TILING_OPTIMAL,
        .usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
    };

    VkImage image_handle;
    TBS_VK_CHECK(vkCreateImage(vulkan_ctx->device_handle, &image_ci, NULL, &image_handle));

    VkDeviceMemory image_mem = tbsvk_AllocateAndBindImageMemory(vulkan_ctx, image_handle, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

    VkImageViewCreateInfo image_view_ci =
    {
        .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
        .image = image_handle,
        .viewType = VK_IMAGE_VIEW_TYPE_2D,
        .format = image_ci.format,
        .components =
        {
            .r = VK_COMPONENT_SWIZZLE_IDENTITY,
            .g = VK_COMPONENT_SWIZZLE_IDENTITY,
            .b = VK_COMPONENT_SWIZZLE_IDENTITY,
            .a = VK_COMPONENT_SWIZZLE_IDENTITY,
        },
        .subresourceRange =
        {
            .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
            .baseMipLevel = 0,
            .levelCount = 1,
            .baseArrayLayer = 0,
            .layerCount = 1,
        },
    };

    VkImageView image_view_handle;
    TBS_VK_CHECK(vkCreateImageView(vulkan_ctx->device_handle, &image_view_ci, NULL, &image_view_handle));

    VkBufferCreateInfo staging_buffer_ci =
    {
        .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .size = image_data->size,
        .usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
    };

    VkBuffer staging_buffer_handle;
    TBS_VK_CHECK(vkCreateBuffer(vulkan_ctx->device_handle, &staging_buffer_ci, NULL, &staging_buffer_handle));

    VkDeviceMemory staging_buffer_mem = tbsvk_AllocateAndBindBufferMemory(vulkan_ctx, staging_buffer_handle, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);

    void *mapped_mem;
    TBS_VK_CHECK(vkMapMemory(vulkan_ctx->device_handle, staging_buffer_mem, 0, VK_WHOLE_SIZE, 0, &mapped_mem));
    memcpy(mapped_mem, image_data->data, image_data->size);

    VkMappedMemoryRange mem_range =
    {
        .sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE,
        .memory = staging_buffer_mem,
        .offset = 0,
        .size = VK_WHOLE_SIZE,
    };

    TBS_VK_CHECK(vkFlushMappedMemoryRanges(vulkan_ctx->device_handle, 1, &mem_range));

    VkCommandBuffer copy_cmd = GetDefaultCmd(vulkan_ctx, CopyBuffer);
    VkCommandBufferBeginInfo cmd_begin_info =
    {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        .flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
    };
    TBS_VK_CHECK(vkBeginCommandBuffer(copy_cmd, &cmd_begin_info));

    VkImageMemoryBarrier from_undef_to_trans_barr =
    {
        .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
        .srcAccessMask = 0,
        .dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT,
        .oldLayout = VK_IMAGE_LAYOUT_UNDEFINED,
        .newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .image = image_handle,
        .subresourceRange =
        {
            .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
            .baseMipLevel = 0,
            .levelCount = 1,
            .baseArrayLayer = 0,
            .layerCount = 1,
        },
    };
    vkCmdPipelineBarrier(copy_cmd,
                         VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT,
                         0,
                         0, NULL,
                         0, NULL,
                         1, &from_undef_to_trans_barr);



    VkBufferImageCopy copy_info =
    {
        .bufferOffset = 0,
        .bufferRowLength = 0,
        .bufferImageHeight = 0,
        .imageSubresource =
        {
            .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
            .mipLevel = 0,
            .baseArrayLayer = 0,
            .layerCount = 1,
        },
        .imageExtent =
        {
            .width = image_data->width,
            .height = image_data->height,
            .depth = 1,
        },
    };
    vkCmdCopyBufferToImage(copy_cmd, staging_buffer_handle, image_handle, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &copy_info);

    VkImageMemoryBarrier from_trans_to_shader_read_barr =
    {
        .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
        .srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT,
        .dstAccessMask = VK_ACCESS_SHADER_READ_BIT,
        .oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        .newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
        .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .image = image_handle,
        .subresourceRange =
        {
            .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
            .baseMipLevel = 0,
            .levelCount = 1,
            .baseArrayLayer = 0,
            .layerCount = 1,
        },
    };

    vkCmdPipelineBarrier(copy_cmd,
                         VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
                         0,
                         0, NULL,
                         0, NULL,
                         1, &from_trans_to_shader_read_barr);

    TBS_VK_CHECK(vkEndCommandBuffer(copy_cmd));

    VkSubmitInfo submit_info =
    {
        .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
        .commandBufferCount = 1,
        .pCommandBuffers = &copy_cmd,
    };

    VkFence fence = tbs_VulkanCreateFence(vulkan_ctx->device_handle);
    TBS_VK_CHECK(vkQueueSubmit(vulkan_ctx->graphics_queue_handle, 1, &submit_info, fence));

    TBS_VK_CHECK(vkWaitForFences(vulkan_ctx->device_handle, 1, &fence, VK_TRUE, UINT64_MAX));
    TBS_VK_CHECK(vkResetFences(vulkan_ctx->device_handle, 1, &fence));
    vkDestroyFence(vulkan_ctx->device_handle, fence, NULL);

    vkDestroyBuffer(vulkan_ctx->device_handle, staging_buffer_handle, NULL);
    vkFreeMemory(vulkan_ctx->device_handle, staging_buffer_mem, NULL);

    tbsvk_Image result = {.image_handle = image_handle, .memory_handle = image_mem, .image_view_handle = image_view_handle};
    return result;
}

static tbsvk_Buffer CreateDirectVertexBuffer(VulkanContext *vulkan_ctx, VertexAttribs *attrib_list, u32 attrib_count)
{
    VkBufferCreateInfo vertex_buffer_ci =
    {
        .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .size = sizeof(VertexAttribs) * attrib_count,
        .usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
    };

    VkBuffer vertex_buffer_handle;
    TBS_VK_CHECK(vkCreateBuffer(vulkan_ctx->device_handle, &vertex_buffer_ci, NULL, &vertex_buffer_handle));

    VkDeviceMemory vert_buffer_mem = tbsvk_AllocateAndBindBufferMemory(vulkan_ctx, vertex_buffer_handle, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);

    void *mapped_mem;
    TBS_VK_CHECK(vkMapMemory(vulkan_ctx->device_handle, vert_buffer_mem, 0, VK_WHOLE_SIZE, 0, &mapped_mem));

    memcpy(mapped_mem, attrib_list, sizeof(VertexAttribs) * attrib_count);

    VkMappedMemoryRange vert_mem_range =
    {
        .sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE,
        .memory = vert_buffer_mem,
        .offset = 0,
        .size = VK_WHOLE_SIZE,
    };
    TBS_VK_CHECK(vkFlushMappedMemoryRanges(vulkan_ctx->device_handle, 1, &vert_mem_range));

    vkUnmapMemory(vulkan_ctx->device_handle, vert_buffer_mem);

    // TODO: handle errors
    tbsvk_Buffer result = {.buffer_handle = vertex_buffer_handle, .memory_handle = vert_buffer_mem};
    return result;
}

static tbsvk_Buffer CreateVertexBufferWithStaging(VulkanContext *vulkan_ctx, VertexAttribs *vert_attrib_list, u32 vert_attrib_count)
{
    VkBufferCreateInfo vert_buffer_ci =
    {
        .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .size = sizeof(VertexAttribs) * vert_attrib_count,
        .usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
    };

    VkBuffer vert_buffer_handle;
    TBS_VK_CHECK(vkCreateBuffer(vulkan_ctx->device_handle, &vert_buffer_ci, NULL, &vert_buffer_handle));

    VkDeviceMemory vert_buffer_mem = tbsvk_AllocateAndBindBufferMemory(vulkan_ctx, vert_buffer_handle, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

    VkBufferCreateInfo staging_buffer_ci =
    {
        .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .size = vert_buffer_ci.size,
        .usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
    };

    VkBuffer staging_buffer_handle;
    TBS_VK_CHECK(vkCreateBuffer(vulkan_ctx->device_handle, &staging_buffer_ci, NULL, &staging_buffer_handle));

    VkDeviceMemory staging_buffer_mem = tbsvk_AllocateAndBindBufferMemory(vulkan_ctx, staging_buffer_handle, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);

    void *mapped_mem;
    TBS_VK_CHECK(vkMapMemory(vulkan_ctx->device_handle, staging_buffer_mem, 0, VK_WHOLE_SIZE, 0, &mapped_mem));
    memcpy(mapped_mem, vert_attrib_list, sizeof(VertexAttribs) * vert_attrib_count);

    VkMappedMemoryRange mem_range =
    {
        .sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE,
        .memory = staging_buffer_mem,
        .offset = 0,
        .size = VK_WHOLE_SIZE,
    };

    TBS_VK_CHECK(vkFlushMappedMemoryRanges(vulkan_ctx->device_handle, 1, &mem_range));

    VkCommandBuffer copy_cmd = GetDefaultCmd(vulkan_ctx, CopyBuffer);
    VkCommandBufferBeginInfo cmd_begin_info =
    {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        .flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
    };
    TBS_VK_CHECK(vkBeginCommandBuffer(copy_cmd, &cmd_begin_info));

    VkBufferCopy copy_info =
    {
        .srcOffset = 0,
        .dstOffset = 0,
        .size = staging_buffer_ci.size,
    };
    vkCmdCopyBuffer(copy_cmd, staging_buffer_handle, vert_buffer_handle, 1, &copy_info);

    VkBufferMemoryBarrier copy_barrier =
    {
        .sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER,
        .srcAccessMask = VK_ACCESS_MEMORY_WRITE_BIT,
        .dstAccessMask = VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT,
        .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .buffer = vert_buffer_handle,
        .offset = 0,
        .size = VK_WHOLE_SIZE,
    };

    vkCmdPipelineBarrier(copy_cmd, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_VERTEX_INPUT_BIT, 0, 0, NULL, 1, &copy_barrier, 0, NULL);

    TBS_VK_CHECK(vkEndCommandBuffer(copy_cmd));

    VkSubmitInfo submit_info =
    {
        .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
        .commandBufferCount = 1,
        .pCommandBuffers = &copy_cmd,
    };

    VkFence fence = tbs_VulkanCreateFence(vulkan_ctx->device_handle);
    TBS_VK_CHECK(vkQueueSubmit(vulkan_ctx->graphics_queue_handle, 1, &submit_info, fence));

    TBS_VK_CHECK(vkWaitForFences(vulkan_ctx->device_handle, 1, &fence, VK_TRUE, UINT64_MAX));
    TBS_VK_CHECK(vkResetFences(vulkan_ctx->device_handle, 1, &fence));
    vkDestroyFence(vulkan_ctx->device_handle, fence, NULL);

    vkDestroyBuffer(vulkan_ctx->device_handle, staging_buffer_handle, NULL);
    vkFreeMemory(vulkan_ctx->device_handle, staging_buffer_mem, NULL);

    tbsvk_Buffer result = {.buffer_handle = vert_buffer_handle, .memory_handle = vert_buffer_mem};
    return result;
}

int main(void)
{
    HMODULE app_instance = GetModuleHandleA(NULL);
    {
        // TODO: Temporary fix for handling file paths. Do a proper pass on paths/filesystem api.
        char binary_path[MAX_PATH] = {0};
        char binary_dir[MAX_PATH] = {0};
        DWORD result_length = GetModuleFileNameA(app_instance, binary_path, MAX_PATH);
        printf("Binary path: %s\n", binary_path);
        assert(result_length > 0);
        assert(GetLastError() == 0);
        size_t i = result_length-1;
        while(i > 0 && binary_path[i] != '\\')
        {
            i--;
        }
        assert(i > 0);
        memcpy(binary_dir, binary_path, i+1);
        printf("Binary dir: %s\n", binary_dir);
        if(!SetCurrentDirectory(binary_dir))
        {
            assert(false);
        }
    }

    WNDCLASSEXA win_class =
    {
        .cbSize = sizeof(WNDCLASSEXA),
        .lpfnWndProc = MainWindowCallbackProc,
        .hInstance = app_instance,
        .lpszClassName = "kvase_window_class",
        .style = CS_VREDRAW | CS_HREDRAW,
    };

    RegisterClassExA(&win_class);
    HWND window_handle = CreateWindowA(win_class.lpszClassName,
                                       "Kvase Engine",
                                       WS_VISIBLE | WS_OVERLAPPEDWINDOW,
                                       CW_USEDEFAULT,
                                       CW_USEDEFAULT,
                                       CW_USEDEFAULT,
                                       CW_USEDEFAULT,
                                       0,
                                       NULL,
                                       app_instance,
                                       NULL);


    HMODULE vulkan_lib = LoadLibrary("../libs/vulkan/bin/vulkan-1.dll");
    if(vulkan_lib)
    {
        printf("Successfully loaded vulkan library\n");
        vkGetInstanceProcAddr = (PFN_vkGetInstanceProcAddr)GetProcAddress(vulkan_lib, "vkGetInstanceProcAddr");
        // TODO: alloc both on heap?
        ApplicationContext app_ctx = {.app_instance = app_instance, .window_handle = window_handle};
        VulkanContext vulkan_ctx = {0};
        if(InitBaseVulkan(vulkan_lib, app_instance, window_handle, &vulkan_ctx))
        {
            VkCommandPoolCreateInfo cmd_pool_ci =
            {
                .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
                .flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
                .queueFamilyIndex = vulkan_ctx.queue_family_index,
            };

            VkCommandPool cmd_pool;
            TBS_VK_CHECK(vkCreateCommandPool(vulkan_ctx.device_handle, &cmd_pool_ci, NULL, &cmd_pool));

#if 0
            {
                VkCommandBufferAllocateInfo cmd_buffer_alloc_info =
                {
                    .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
                    .commandPool = cmd_pool,
                    .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
                    .commandBufferCount = 1,
                };
                VkCommandBuffer cmd_buffer;
                TBS_VK_CHECK(vkAllocateCommandBuffers(vulkan_ctx.device_handle, &cmd_buffer_alloc_info, &cmd_buffer));
                vulkan_ctx.default_cmd_buffers[DefaultCmdType_DepthSetup] = cmd_buffer;
            }
#endif

            {
                VkCommandBufferAllocateInfo setup_cmd_alloc_info =
                {
                    .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
                    .commandPool = cmd_pool,
                    .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
                    .commandBufferCount = 1,
                };

                VkCommandBuffer setup_cmd;
                TBS_VK_CHECK(vkAllocateCommandBuffers(vulkan_ctx.device_handle, &setup_cmd_alloc_info, &setup_cmd));
                vulkan_ctx.default_cmd_buffers[DefaultCmdType_SwapchainSetup] = setup_cmd;
            }

            {
                VkCommandBufferAllocateInfo render_cmd_alloc_info =
                {
                    .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
                    .commandPool = cmd_pool,
                    .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
                    .commandBufferCount = 1,
                };

                VkCommandBuffer render_cmd;
                TBS_VK_CHECK(vkAllocateCommandBuffers(vulkan_ctx.device_handle, &render_cmd_alloc_info, &render_cmd));
                vulkan_ctx.default_cmd_buffers[DefaultCmdType_Render] = render_cmd;
            }

            {
                VkCommandBufferAllocateInfo image_layout_cmd_alloc_info =
                {
                    .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
                    .commandPool = cmd_pool,
                    .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
                    .commandBufferCount = 1,
                };

                VkCommandBuffer image_layout_cmd;
                TBS_VK_CHECK(vkAllocateCommandBuffers(vulkan_ctx.device_handle, &image_layout_cmd_alloc_info, &image_layout_cmd));
                vulkan_ctx.default_cmd_buffers[DefaultCmdType_ImageLayout] = image_layout_cmd;
            }

            {
                VkCommandBufferAllocateInfo copy_cmd_alloc_info =
                {
                    .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
                    .commandPool = cmd_pool,
                    .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
                    .commandBufferCount = 1,
                };

                VkCommandBuffer copy_cmd;
                TBS_VK_CHECK(vkAllocateCommandBuffers(vulkan_ctx.device_handle, &copy_cmd_alloc_info, &copy_cmd));
                vulkan_ctx.default_cmd_buffers[DefaultCmdType_CopyBuffer] = copy_cmd;
            }

            {
                VkCommandBufferAllocateInfo compute_cmd_alloc_info =
                {
                    .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
                    .commandPool = cmd_pool,
                    .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
                    .commandBufferCount = 1,
                };

                VkCommandBuffer compute_cmd;
                TBS_VK_CHECK(vkAllocateCommandBuffers(vulkan_ctx.device_handle, &compute_cmd_alloc_info, &compute_cmd));
                vulkan_ctx.default_cmd_buffers[DefaultCmdType_Compute] = compute_cmd;
            }

            CreateAndInitSwapchain(&app_ctx, &vulkan_ctx);


            // TODO(torgrim): Here we actually need to check that this is supported
            // by the surface
            VkFormat default_depth_format = VK_FORMAT_D16_UNORM;
            //CreateAndInitDepthImageView(&vulkan_ctx, default_depth_format);
            CreateDefaultRenderPass(&vulkan_ctx, default_depth_format);
            CreateDefaultFramebuffers(&vulkan_ctx);

            FileData vert_source = debug_win32_ReadFile("../code/shaders/spirv/scene_vert.spv");
            FileData frag_source = debug_win32_ReadFile("../code/shaders/spirv/scene_frag.spv");
            FileData compute_source = debug_win32_ReadFile("../code/shaders/spirv/compute.spv");

            assert(vert_source.size % 4 == 0);
            assert(frag_source.size % 4 == 0);
            assert(compute_source.size % 4 == 0);

            VkShaderModuleCreateInfo vert_shader_module_ci =
            {
                .sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
                .codeSize = vert_source.size,
                .pCode = (u32 *)vert_source.data,
            };

            VkShaderModuleCreateInfo frag_shader_module_ci =
            {
                .sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
                .codeSize = frag_source.size,
                .pCode = (u32 *)frag_source.data,
            };
            VkShaderModuleCreateInfo compute_shader_module_ci =
            {
                .sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
                .codeSize = compute_source.size,
                .pCode = (u32 *)compute_source.data,
            };

            VkShaderModule vert_shader_module;
            VkShaderModule frag_shader_module;
            VkShaderModule compute_shader_module;
            TBS_VK_CHECK(vkCreateShaderModule(vulkan_ctx.device_handle, &vert_shader_module_ci, NULL, &vert_shader_module));
            TBS_VK_CHECK(vkCreateShaderModule(vulkan_ctx.device_handle, &frag_shader_module_ci, NULL, &frag_shader_module));
            TBS_VK_CHECK(vkCreateShaderModule(vulkan_ctx.device_handle, &compute_shader_module_ci, NULL, &compute_shader_module));

            debug_win32_FreeFileData(&vert_source);
            debug_win32_FreeFileData(&frag_source);

            VertexAttribs vertex_data[] =
            {
                {.pos = {.x = -0.5f, .y =  0.5f}, .uv = {.x = 0.0f, .y = 1.0f}, .col = {.r = 1.0f, .a = 1.0f}},
                {.pos = {.x =  0.5f, .y =  0.5f}, .uv = {.x = 1.0f, .y = 1.0f}, .col = {.g = 1.0f, .a = 1.0f}},
                {.pos = {.x =  0.5f, .y = -0.5f}, .uv = {.x = 1.0f, .y = 0.0f}, .col = {.b = 1.0f, .a = 1.0f}},

                {.pos = {.x =  0.5f, .y = -0.5f}, .uv = {.x = 1.0f, .y = 0.0f}, .col = {.b = 1.0f, .a = 1.0f}},
                {.pos = {.x = -0.5f, .y = -0.5f}, .uv = {.x = 0.0f, .y = 0.0f}, .col = {.r = 1.0f, .b = 1.0f, .a = 1.0f}},
                {.pos = {.x = -0.5f, .y =  0.5f}, .uv = {.x = 0.0f, .y = 1.0f}, .col = {.r = 1.0f, .a = 1.0f}},
            };

            // tbs_VulkanBuffer vertex_buffer = CreateDirectVertexBuffer(&vulkan_ctx, vertex_data, array_count(vertex_data));
            tbsvk_Buffer vertex_buffer = CreateVertexBufferWithStaging(&vulkan_ctx, vertex_data, array_count(vertex_data));

            {
                VkSemaphore present_done_sem;
                VkSemaphore render_done_sem;
                VkSemaphoreCreateInfo sem_ci = {.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO};
                vkCreateSemaphore(vulkan_ctx.device_handle, &sem_ci, NULL, &present_done_sem);
                vkCreateSemaphore(vulkan_ctx.device_handle, &sem_ci, NULL, &render_done_sem);

                vulkan_ctx.default_semaphores[DefaultSemaphores_PresentDone] = present_done_sem;
                vulkan_ctx.default_semaphores[DefaultSemaphores_RenderDone] = render_done_sem;
            }


            CircleList circle_list = {0};
            tbsvk_Buffer circle_buffer = CreateStorageBufferDefault(&vulkan_ctx, sizeof(CircleList));

            {
                circle_list.data[0] = (Circle){.x = 0.0f, .y = 0.0f, .radius = 1000.0f};
#if 0
                circle_list.data[1] = (Circle){.x = 10.0f, .y = 20.0f, .radius = 5.0f};
                circle_list.data[2] = (Circle){.x = 300.0f, .y = 530.0f, .radius = 900.0f};
                circle_list.count++;
                circle_list.count++;
                circle_list.count++;
#endif

                void *mapped = NULL;
                vkMapMemory(vulkan_ctx.device_handle, circle_buffer.memory_handle, 0, VK_WHOLE_SIZE, 0, &mapped);

                memcpy(mapped, &circle_list, sizeof(CircleList));
            }

            VkDescriptorSet compute_desc_set = VK_NULL_HANDLE;
            VkDescriptorSetLayout compute_desc_set_layout = VK_NULL_HANDLE;
            tbsvk_Image compute_target = {0};
            {
                VkSamplerCreateInfo sampler_ci =
                {
                    .sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
                    .minFilter = VK_FILTER_LINEAR,
                    .magFilter = VK_FILTER_LINEAR,
                    .mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR,
                    .addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
                    .addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
                    .addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
                    .borderColor = VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE,
                    .compareOp = VK_COMPARE_OP_NEVER,
                    .maxAnisotropy = 1.0f,
                };

                VkSampler sampler_handle;
                TBS_VK_CHECK(vkCreateSampler(vulkan_ctx.device_handle, &sampler_ci, NULL, &sampler_handle));

                VkDescriptorPoolSize desc_pool_sizes[] =
                {
                    {
                        .type = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE,
                        .descriptorCount = 1,
                    },
                    {
                        .type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
                        .descriptorCount = 1,
                    },
                };

                VkDescriptorPoolCreateInfo desc_pool_ci =
                {
                    .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
                    .maxSets = 1,
                    .poolSizeCount = array_count(desc_pool_sizes),
                    .pPoolSizes = desc_pool_sizes,
                };

                VkDescriptorPool compute_desc_pool;
                TBS_VK_CHECK(vkCreateDescriptorPool(vulkan_ctx.device_handle, &desc_pool_ci, NULL, &compute_desc_pool));
                VkDescriptorSetLayoutBinding desc_set_layout_bindings[] =
                {
                    {
                        .binding = 0,
                        .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE,
                        .descriptorCount = 1,
                        .stageFlags = VK_SHADER_STAGE_COMPUTE_BIT,
                    },
                    {
                        .binding = 1,
                        .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
                        .descriptorCount = 1,
                        .stageFlags = VK_SHADER_STAGE_COMPUTE_BIT,
                    },
                };

                VkDescriptorSetLayoutCreateInfo desc_set_layout_ci =
                {
                    .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
                    .bindingCount = array_count(desc_set_layout_bindings),
                    .pBindings = desc_set_layout_bindings,
                };

                VkDescriptorSetLayout desc_set_layout;
                TBS_VK_CHECK(vkCreateDescriptorSetLayout(vulkan_ctx.device_handle, &desc_set_layout_ci, NULL, &desc_set_layout));

                VkDescriptorSetAllocateInfo desc_set_alloc_info =
                {
                    .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
                    .descriptorPool = compute_desc_pool,
                    .descriptorSetCount = 1,
                    .pSetLayouts = &desc_set_layout,
                };
                VkDescriptorSet desc_set;
                TBS_VK_CHECK(vkAllocateDescriptorSets(vulkan_ctx.device_handle, &desc_set_alloc_info, &desc_set));


                VkDescriptorSetLayout desc_set_layouts[] =
                {
                    desc_set_layout,
                };

                tbsvk_Image target = CreateImageTargetDefault2D(&vulkan_ctx);

                VkDescriptorImageInfo desc_image_infos[] =
                {
                    {
                        .sampler = sampler_handle,
                        .imageView = target.image_view_handle,
                        .imageLayout = VK_IMAGE_LAYOUT_GENERAL,
                    },
                };


                VkDescriptorBufferInfo desc_buffer_infos[] =
                {
                    {
                        .buffer = circle_buffer.buffer_handle,
                        .offset = 0,
                        .range = VK_WHOLE_SIZE,
                    },
                };

                VkWriteDescriptorSet descriptor_writes[] =
                {
                    {
                        .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
                        .dstSet = desc_set,
                        .dstBinding = 0,
                        .dstArrayElement = 0,
                        .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE,
                        .descriptorCount = array_count(desc_image_infos),
                        .pImageInfo = desc_image_infos,
                    },
                    {
                        .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
                        .dstSet = desc_set,
                        .dstBinding = 1,
                        .dstArrayElement = 0,
                        .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
                        .descriptorCount = array_count(desc_buffer_infos),
                        .pBufferInfo = desc_buffer_infos,
                    },
                };

                vkUpdateDescriptorSets(vulkan_ctx.device_handle,
                                       array_count(descriptor_writes), descriptor_writes,
                                       0, NULL);


                compute_desc_set = desc_set;
                compute_desc_set_layout = desc_set_layout;
                compute_target = target;
            }

            CreateDefaultComputePipeline(&vulkan_ctx, compute_shader_module, &compute_desc_set_layout, 1);

#if 1
            VkDescriptorSetLayout desc_set_layouts[1] = {0};
            VkDescriptorSet desc_sets[1] = {0};
            {
                VkSamplerCreateInfo sampler_ci =
                {
                    .sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
                    .minFilter = VK_FILTER_LINEAR,
                    .magFilter = VK_FILTER_LINEAR,
                    .mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR,
                    .addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
                    .addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
                    .addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
                    .borderColor = VK_BORDER_COLOR_FLOAT_TRANSPARENT_BLACK,
                };
                VkSampler sampler_handle;
                TBS_VK_CHECK(vkCreateSampler(vulkan_ctx.device_handle, &sampler_ci, NULL, &sampler_handle));

                VkDescriptorPoolSize desc_pool_sizes[] =
                {
                    {
                        .type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                        .descriptorCount = 1,
                    },
                };

                VkDescriptorPoolCreateInfo desc_pool_ci =
                {
                    .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
                    .maxSets = 1,
                    .poolSizeCount = array_count(desc_pool_sizes),
                    .pPoolSizes = desc_pool_sizes,
                };
                VkDescriptorPool default_desc_pool;
                TBS_VK_CHECK(vkCreateDescriptorPool(vulkan_ctx.device_handle, &desc_pool_ci, NULL, &default_desc_pool));

                VkDescriptorSetLayoutBinding desc_set_layout_binding =
                {
                    .binding = 0,
                    .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                    .descriptorCount = 1,
                    .stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
                };

                VkDescriptorSetLayoutCreateInfo desc_set_layout_ci =
                {
                    .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
                    .bindingCount = 1,
                    .pBindings = &desc_set_layout_binding,
                };

                VkDescriptorSetLayout desc_set_layout;
                TBS_VK_CHECK(vkCreateDescriptorSetLayout(vulkan_ctx.device_handle, &desc_set_layout_ci, NULL, &desc_set_layout));

                VkDescriptorSetAllocateInfo desc_alloc_info =
                {
                    .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
                    .descriptorPool = default_desc_pool,
                    .descriptorSetCount = 1,
                    .pSetLayouts = &desc_set_layout,
                };
                VkDescriptorSet desc_set;
                TBS_VK_CHECK(vkAllocateDescriptorSets(vulkan_ctx.device_handle, &desc_alloc_info, &desc_set));

                VkDescriptorImageInfo desc_image_infos[] =
                {
                    {
                        .sampler = sampler_handle,
                        .imageView = compute_target.image_view_handle,
                        .imageLayout = VK_IMAGE_LAYOUT_GENERAL,
                    },
                };
                VkWriteDescriptorSet descriptor_writes[] =
                {
                    {
                        .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
                        .dstSet = desc_set,
                        .dstBinding = 0,
                        .dstArrayElement = 0,
                        .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                        .descriptorCount = array_count(desc_image_infos),
                        .pImageInfo = desc_image_infos,
                    },
                };

                vkUpdateDescriptorSets(vulkan_ctx.device_handle,
                                       array_count(descriptor_writes), descriptor_writes,
                                       0, NULL);

                desc_set_layouts[0] = desc_set_layout;
                desc_sets[0] = desc_set;
            }
#endif

            CreateDefaultGraphicsPipeline(&vulkan_ctx, vert_shader_module, frag_shader_module, desc_set_layouts, array_count(desc_set_layouts));
            //CreateDefaultGraphicsPipeline(&vulkan_ctx, vert_shader_module, frag_shader_module, NULL, 0);

            VkFence compute_fence;
            TBS_VK_CHECK(vkCreateFence(vulkan_ctx.device_handle, &sig_fence_ci, NULL, &compute_fence));
            VkFence render_fence;
            TBS_VK_CHECK(vkCreateFence(vulkan_ctx.device_handle, &unsig_fence_ci, NULL, &render_fence));

            bool app_running = true;
            while(app_running)
            {
                MSG msg;
                while(PeekMessageA(&msg, NULL, 0, 0, PM_REMOVE))
                {
                    switch(msg.message)
                    {
                        case WM_QUIT:
                        {
                            app_running = false;
                        } break;
                        default:
                        {
                            TranslateMessage(&msg);
                            DispatchMessage(&msg);
                        }
                    }
                }

                // TODO: handle this better
                if(!app_running)
                {
                    continue;
                }

                V2U win_size = win32_GetWindowSizeU32(app_ctx.window_handle);

                // TODO: handle this better
                if(win_size.width == 0 || win_size.height == 0)
                {
                    continue;
                }
                V2U ext_size = {.width = vulkan_ctx.swapchain_extent.width, .height = vulkan_ctx.swapchain_extent.height};
                if(!v2u_Equal(win_size, ext_size))
                {
                    assert(win_size.width > 0 && win_size.height > 0);
                    DestroySwapchainResources(&vulkan_ctx);
                    CreateAndInitSwapchain(&app_ctx, &vulkan_ctx);
                    //CreateAndInitDepthImageView(&vulkan_ctx, default_depth_format);
                    CreateDefaultGraphicsPipeline(&vulkan_ctx, vert_shader_module, frag_shader_module, desc_set_layouts, array_count(desc_set_layouts));
                    //CreateDefaultGraphicsPipeline(&vulkan_ctx, vert_shader_module, frag_shader_module, NULL, 0);
                    CreateDefaultFramebuffers(&vulkan_ctx);
                }


                VkSemaphore present_done_sem = GetDefaultSemaphore(&vulkan_ctx, PresentDone);
                VkSemaphore render_done_sem = GetDefaultSemaphore(&vulkan_ctx, RenderDone);

                u32 next_image_index;
                TBS_VK_CHECK(vkAcquireNextImageKHR(vulkan_ctx.device_handle,
                                                   vulkan_ctx.swapchain_handle,
                                                   UINT64_MAX,
                                                   present_done_sem,
                                                   VK_NULL_HANDLE,
                                                   &next_image_index));


                {
                    VkCommandBuffer render_cmd = GetDefaultCmd(&vulkan_ctx, Render);

                    VkCommandBufferBeginInfo present_cmd_begin_info =
                    {
                        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
                        .flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
                    };

                    TBS_VK_CHECK(vkBeginCommandBuffer(render_cmd, &present_cmd_begin_info));

                    VkImageMemoryBarrier layout_trans_barrier =
                    {
                        .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
                        .srcAccessMask = VK_ACCESS_MEMORY_READ_BIT,
                        .dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
                        .oldLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
                        .newLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
                        .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
                        .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
                        .image = vulkan_ctx.swapchain_images[next_image_index],
                        .subresourceRange =
                        {
                            .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                            .baseMipLevel = 0,
                            .levelCount = 1,
                            .baseArrayLayer = 0,
                            .layerCount = 1,
                        },
                    };

                    vkCmdPipelineBarrier(render_cmd,
                                         VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
                                         VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
                                         0,
                                         0, NULL,
                                         0, NULL,
                                         1, &layout_trans_barrier);


                    VkImageMemoryBarrier compute_image_done_barr =
                    {
                        .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
                        .srcAccessMask = VK_ACCESS_SHADER_WRITE_BIT,
                        .dstAccessMask = VK_ACCESS_SHADER_READ_BIT,
                        .oldLayout = VK_IMAGE_LAYOUT_GENERAL,
                        .newLayout = VK_IMAGE_LAYOUT_GENERAL,
                        .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
                        .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
                        .image = compute_target.image_handle,
                        .subresourceRange =
                        {
                            .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                            .baseMipLevel = 0,
                            .levelCount = 1,
                            .baseArrayLayer = 0,
                            .layerCount = 1,
                        },
                    };

                    vkCmdPipelineBarrier(render_cmd,
                                         VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT,
                                         VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
                                         0,
                                         0, NULL,
                                         0, NULL,
                                         1, &compute_image_done_barr);

                    VkClearValue clear_values[] =
                    {
                        {.color = {.float32 = {0.0f, 0.0f, 0.0f, 1.0f}}},
                        {.depthStencil = {.depth = 1.0f, .stencil = 0}},
                    };
                    VkRenderPassBeginInfo render_pass_begin =
                    {
                        .sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
                        .renderPass = vulkan_ctx.default_render_pass_handle,
                        .framebuffer = vulkan_ctx.framebuffers[next_image_index],
                        .renderArea = {.offset = {0,0}, .extent = vulkan_ctx.swapchain_extent},
                        .clearValueCount = array_count(clear_values),
                        .pClearValues = clear_values,
                    };

                    vkCmdBeginRenderPass(render_cmd, &render_pass_begin, VK_SUBPASS_CONTENTS_INLINE);
                    vkCmdBindPipeline(render_cmd, VK_PIPELINE_BIND_POINT_GRAPHICS, vulkan_ctx.graphics_pipeline_handle);
                    VkViewport viewport =
                    {
                        .x = 0,
                        .y = 0,
                        .width = (f32)vulkan_ctx.swapchain_extent.width,
                        .height = (f32)vulkan_ctx.swapchain_extent.height,
                        .minDepth = 0.0f,
                        .maxDepth = 0.0f,
                    };

                    vkCmdSetViewport(render_cmd, 0, 1, &viewport);

                    VkRect2D scissor = {.offset = {0, 0}, .extent = vulkan_ctx.swapchain_extent};

                    vkCmdSetScissor(render_cmd, 0, 1, &scissor);

                    VkDeviceSize offset = 0;
                    vkCmdBindVertexBuffers(render_cmd, 0, 1, &vertex_buffer.buffer_handle, &offset);
#if 1
                    vkCmdBindDescriptorSets(render_cmd,
                                            VK_PIPELINE_BIND_POINT_GRAPHICS,
                                            vulkan_ctx.graphics_pipeline_layout_handle,
                                            0,
                                            array_count(desc_sets),
                                            desc_sets,
                                            0,
                                            NULL);
#endif

                    vkCmdDraw(render_cmd, array_count(vertex_data), 1, 0, 0);

                    vkCmdEndRenderPass(render_cmd);

                    VkImageMemoryBarrier present_trans_barrier =
                    {
                        .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
                        .srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
                        .dstAccessMask = VK_ACCESS_MEMORY_READ_BIT,
                        .oldLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
                        .newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
                        .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
                        .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
                        .image = vulkan_ctx.swapchain_images[next_image_index],
                        .subresourceRange =
                        {
                            .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                            .baseMipLevel = 0,
                            .levelCount = 1,
                            .baseArrayLayer = 0,
                            .layerCount = 1,
                        },
                    };

                    vkCmdPipelineBarrier(render_cmd,
                                         VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
                                         VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
                                         0,
                                         0, NULL,
                                         0, NULL,
                                         1, &present_trans_barrier);

                    TBS_VK_CHECK(vkEndCommandBuffer(render_cmd));


                    VkPipelineStageFlags wait_stage_mask = {VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT};
                    VkSubmitInfo render_submit_info =
                    {
                        .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
                        .waitSemaphoreCount = 1,
                        .pWaitSemaphores = &present_done_sem,
                        .pWaitDstStageMask = &wait_stage_mask,
                        .signalSemaphoreCount = 1,
                        .pSignalSemaphores = &render_done_sem,
                        .commandBufferCount = 1,
                        .pCommandBuffers = &render_cmd,
                    };
                    TBS_VK_CHECK(vkQueueSubmit(vulkan_ctx.graphics_queue_handle, 1, &render_submit_info, render_fence));

                    TBS_VK_CHECK(vkWaitForFences(vulkan_ctx.device_handle, 1, &render_fence, VK_TRUE, UINT64_MAX));
                    TBS_VK_CHECK(vkResetFences(vulkan_ctx.device_handle, 1, &render_fence));

                    VkPresentInfoKHR present_info =
                    {
                        .sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
                        .waitSemaphoreCount = 1,
                        .pWaitSemaphores = &render_done_sem,
                        .swapchainCount = 1,
                        .pSwapchains = &vulkan_ctx.swapchain_handle,
                        .pImageIndices = &next_image_index,
                    };

                    TBS_VK_CHECK(vkQueuePresentKHR(vulkan_ctx.graphics_queue_handle, &present_info));

                    TBS_VK_CHECK(vkResetCommandBuffer(render_cmd, 0));

#if 1
                    TBS_VK_CHECK(vkWaitForFences(vulkan_ctx.device_handle, 1, &compute_fence, VK_TRUE, UINT64_MAX));
                    TBS_VK_CHECK(vkResetFences(vulkan_ctx.device_handle, 1, &compute_fence));
                    // NOTE: This command buffer only need to be rebuilt if something
                    // about the description changes(like window resize etc)
                    VkCommandBuffer compute_cmd = GetDefaultCmd(&vulkan_ctx, Compute);
                    TBS_VK_CHECK(vkBeginCommandBuffer(compute_cmd, &default_cmd_begin_info));

                    vkCmdBindPipeline(compute_cmd, VK_PIPELINE_BIND_POINT_COMPUTE, vulkan_ctx.compute_pipeline_handle);
                    vkCmdBindDescriptorSets(compute_cmd,
                                            VK_PIPELINE_BIND_POINT_COMPUTE,
                                            vulkan_ctx.compute_pipeline_layout_handle,
                                            0,
                                            1, &compute_desc_set,
                                            0, 0);

                    vkCmdDispatch(compute_cmd, vulkan_ctx.swapchain_extent.width / 16, vulkan_ctx.swapchain_extent.height / 16, 1);

                    vkEndCommandBuffer(compute_cmd);

                    VkSubmitInfo compute_submit_info =
                    {
                        .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
                        .waitSemaphoreCount = 0,
                        .pWaitSemaphores = 0,
                        .pWaitDstStageMask = 0,
                        .signalSemaphoreCount = 0,
                        .pSignalSemaphores = 0,
                        .commandBufferCount = 1,
                        .pCommandBuffers = &compute_cmd,
                    };
                    TBS_VK_CHECK(vkQueueSubmit(vulkan_ctx.compute_queue_handle, 1, &compute_submit_info, compute_fence));
#endif
                }
            }
        }
    }

    return 0;
}
